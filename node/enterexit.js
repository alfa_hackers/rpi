var noble = require('noble');

var RSSI_THRESHOLD    = -50;
var EXIT_GRACE_PERIOD = 1000; // milliseconds

var inRange = [];

console.log('Hello World');

noble.on('discover', function(peripheral) {
  if (peripheral.rssi < RSSI_THRESHOLD) {
    // ignore
    return;
  }

  var id = peripheral.id;
  var entered = !inRange[id];

  if (entered) {
    inRange[id] = {
      peripheral: peripheral
    };
	console.log('=========================================================');
	console.log('🙏  '+ peripheral.advertisement.localName + ' welcome 🏠');
	console.log('ID = ' + id);
	console.log('address = ' + peripheral.address);
	console.log('addressType = ' + peripheral.addressType);
	console.log('connectable = ' + peripheral.connectable);
	console.log('rssi = ' + peripheral.rssi);
	console.log('services = ' + peripheral.services);
	console.log('state = ' + peripheral.state);
	console.log('advertisement = ' + peripheral.advertisement);
	console.log('=========================================================');
    
  }

  inRange[id].lastSeen = Date.now();
});

setInterval(function() {
  for (var id in inRange) {
    if (inRange[id].lastSeen < (Date.now() - EXIT_GRACE_PERIOD)) {
      var peripheral = inRange[id].peripheral;
    	console.log('=========================================================');
		console.log('Good bye ' + peripheral.advertisement.localName + ' 👋 ')
		console.log('=========================================================');
// 	  console.log('=========================================================');
//       console.log('"' + peripheral.advertisement.localName + '" exited (RSSI ' + peripheral.rssi + ') DATE : ' + new Date());
// 	  console.log('=========================================================');
      delete inRange[id];
    }
  }
}, EXIT_GRACE_PERIOD / 2);

noble.on('stateChange', function(state) {
  if (state === 'poweredOn') {
	console.log('Bluetooth device turned on');
    noble.startScanning([], true);
  } else {
	console.log('Bluetooth device turned off');
    noble.stopScanning();
  }
});