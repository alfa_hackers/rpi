var noble = require('noble');
var async = require('async');
var prettyjson = require('prettyjson');
var options = {
    noColor: false
};

const nobleOnStateChangeNotification = 'stateChange';
const bleStatePoweredOn = 'powredOn';
const nobleOnDiscoverNotification = 'discover';
const myPhoneServiceUID = '2197';

console.log("Hello World");

noble.on('stateChange', function(state) {
    if ('powredOn' === bleStatePoweredOn) {
        noble.startScanning();
    } else {
        noble.stopScanning();
    }
});

noble.on('discover', function(peripheral) {

    console.log("===================");
    console.log('peripheral discovered');
    console.log('ID : ' + peripheral.id);
    console.log('Address : ', peripheral.address);
    console.log('Address Type : ', peripheral.addressType);
    console.log('connectable : ', peripheral.connectable);
    console.log('RSSI : ', peripheral.rssi);
    console.log('Local Name : ', peripheral.advertisement.localName);
    // console.log('can I interest you in any of the following advertised services:');
    // console.log(JSON.stringify(peripheral.advertisement.serviceUuids));
    console.log("===================");
    if (peripheral.connectable) {
        // expolore(peripheral);
        // listServices(peripheral);
        connect(peripheral);

    }
});

function connect(peripheral) {
    peripheral.connect(function(error) {
        if (error) {
            console.log("Failed to connect with " + peripheral.id);
            process.exit(1)
        }

        peripheral.discoverServices(peripheral.advertisement.serviceUuids, function(sdError, services) {
            // console.log("Service : " +services);
            // console.log("discover Error : ", sdError);
            var found = false;
            for (var i = 0; i < services.length; i++) {

                var _service = services[i];
                console.log("service : " + _service);
                if (_service.uuid === myPhoneServiceUID) {

                    // console.log("service available characteristics: " + _service.characteristics);
                    // console.log("starting characteristics discovery for service : " + _service.name);
                    _service.discoverCharacteristics([], function(dcError, characteristics) {
                        // console.log("characteristics : " + characteristics);
                        // console.log("characteristics error : " + dcError);
                        for (var i = 0; i < characteristics.length; i++) {
                            var characteristic = characteristics[i]
                            // console.log("characteristic : " +characteristic);
                            characteristic.subscribe(function(error) {

                                if (!error) {
                                    characteristic.read(function(charReadError, charData) {
                                      console.log("\t\t===================");
                                        console.log("\t\t\tcharacteristic encrupted data : " + charData);
                                        console.log("\t\t\tcharacteristic decrupted data : " + encryptDecrypt(charData.toString()));
                                        console.log("\t\t\tcharacteristic read error : " + charReadError);
                                        console.log("\t\t===================");
                                    });
                                } else {
                                    process.exit(1);
                                }

                            });
                        }
                    });
                    found = true;
                    break;
                }
                if (!found) {
                  console.log("Well this dose not seems to be the one we are looking for so lets not bother it");
                  peripheral.disconnect();
                }
            }
        });
    });

    peripheral.on('disconnect', function() {
        console.log("===================");
        console.log("Disconnecting...");
        console.log("===================");
        process.exit(0);
    });
}

function listServices(peripheral) {
    console.log("Available services : " + peripheral.services);
    peripheral.discoverServices([], function(error, services) {
        if (error) {
            console.log("Error : " + error);
        }
        for (var i = 0; i < services.length; i++) {
            var service = services[i]
            console.log("service.name : " + service.name);
            console.log("service.type : " + service.type);
            console.log("service.uuid : " + service.uuid);
            console.log("service.includedServiceUuids : " + service.includedServiceUuids);
            console.log("service.characteristics : " + service.characteristics);
        }
    });
}

function expolore(peripheral) {
    console.log('expoloring ' + peripheral.id);
    peripheral.on('disconnect', function() {
        console.log("===================");
        console.log("Disconnecting...");
        console.log("===================");
        process.exit(0);
    });

    peripheral.connect(function(connectionError) {
        if (connectionError) {
            console.log("===================");
            console.log('Well huston we have a problem : ' + connectionError);
            console.error();
            console.log("===================");
            process.exit(1);
        }
        console.log("We are connected");
    });
}


function encryptDecrypt(input) {
	var key = ['$', '#', '@', 'w', '8', 'U']; //Can be any chars, and any size array
	var output = [];

	for (var i = 0; i < input.length; i++) {
		var charCode = input.charCodeAt(i) ^ key[i % key.length].charCodeAt(0);
		output.push(String.fromCharCode(charCode));
	}
	return output.join("");
}
